package com.aras.modir.imdbmvvm.ModelForDatabase.Dao;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

public interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T... entity);

    @Delete
    void deleteall(T... entity);

    @Update
    void update(T... entity);
}
