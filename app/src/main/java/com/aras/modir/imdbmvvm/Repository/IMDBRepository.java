package com.aras.modir.imdbmvvm.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.Intent;

import com.aras.modir.imdbmvvm.ModelForDatabase.Dao.IMDBPojoDao;
import com.aras.modir.imdbmvvm.ModelForDatabase.Dao.RatingDao;
import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.IMDBPojo;
import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.Rating;
import com.aras.modir.imdbmvvm.ModelForDatabase.RoomDatabase;
import com.aras.modir.imdbmvvm.Network.API;
import com.aras.modir.imdbmvvm.Network.WebService;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class IMDBRepository {

    private IMDBPojoDao imdbPojoDao;
    private RatingDao ratingDao;

    private LiveData<IMDBPojo> imdbPojoLiveData;
    private LiveData<List<Rating>> ratingLiveData;

    public IMDBRepository(Application application) {

        RoomDatabase database = RoomDatabase.getAppDatabase(application);

        imdbPojoDao = database.imdbPojoDao();
        ratingDao = database.ratingDao();

        imdbPojoLiveData = imdbPojoDao.getAllIMDB();
        ratingLiveData = ratingDao.getAllRating();
    }

    public LiveData<IMDBPojo> getAllIMDB() {
        API.getMovie().create(WebService.class).searchInIMDB("gladiator", "70ad462a")
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<IMDBPojo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(IMDBPojo imdbPojo) {
                        imdbPojoDao.insert(imdbPojo);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return imdbPojoDao.getAllIMDB();
    }
}
