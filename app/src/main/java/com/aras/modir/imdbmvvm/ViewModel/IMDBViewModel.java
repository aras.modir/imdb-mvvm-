package com.aras.modir.imdbmvvm.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.IMDBPojo;
import com.aras.modir.imdbmvvm.Repository.IMDBRepository;

public class IMDBViewModel extends AndroidViewModel {


    private LiveData<IMDBPojo> imdbPojoLiveData;

    private IMDBRepository imdbRepository;

    public IMDBViewModel(@NonNull Application application) {
        super(application);
        imdbRepository = new IMDBRepository(application);
        imdbPojoLiveData = imdbRepository.getAllIMDB();
    }

    public LiveData<IMDBPojo> getImdbPojoLiveData() {
        return imdbPojoLiveData;
    }
}
