package com.aras.modir.imdbmvvm;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.IMDBPojo;
import com.aras.modir.imdbmvvm.ViewModel.IMDBViewModel;
import com.aras.modir.imdbmvvm.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements Observer<IMDBPojo> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        final ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        IMDBViewModel viewModel = ViewModelProviders.of(this).get(IMDBViewModel.class);
        viewModel.getImdbPojoLiveData().observe(this, this);
    }

    @Override
    public void onChanged(@Nullable IMDBPojo imdbPojo) {
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.text.setText(imdbPojo.getActors());
        binding.director.setText(imdbPojo.getDirector());
    }
}
