package com.aras.modir.imdbmvvm.Network;

import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.IMDBPojo;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebService {

    @GET("/")
    Observable<IMDBPojo>
    searchInIMDB(@Query("t") String t, @Query("apikey") String apikey);

}
