package com.aras.modir.imdbmvvm.ModelForDatabase;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.aras.modir.imdbmvvm.ModelForDatabase.Dao.IMDBPojoDao;
import com.aras.modir.imdbmvvm.ModelForDatabase.Dao.RatingDao;
import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.IMDBPojo;
import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.Rating;

@Database(entities = {IMDBPojo.class, Rating.class}, version = 1, exportSchema = false)
@TypeConverters({RelatedContentConverter.class})
public abstract class RoomDatabase extends android.arch.persistence.room.RoomDatabase {

    private static RoomDatabase INSTANCE;

    public abstract IMDBPojoDao imdbPojoDao();

    public abstract RatingDao ratingDao();

    public static RoomDatabase getAppDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabase.class, "AllIMDB_database")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
