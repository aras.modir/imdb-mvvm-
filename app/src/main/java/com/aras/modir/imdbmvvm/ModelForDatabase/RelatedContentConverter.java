package com.aras.modir.imdbmvvm.ModelForDatabase;

import android.arch.persistence.room.TypeConverter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.Rating;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class RelatedContentConverter {

    @TypeConverter
    public static String relatedContentsToString(List<Rating> ratings) {
        return ratings.toString();
    }

    @TypeConverter
    public static List<Rating> relatedContentsToList(String relcont) {
        return new List<Rating>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(@Nullable Object o) {
                return false;
            }

            @NonNull
            @Override
            public Iterator<Rating> iterator() {
                return null;
            }

            @Nullable
            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(@Nullable T[] a) {
                return null;
            }

            @Override
            public boolean add(Rating rating) {
                return false;
            }

            @Override
            public boolean remove(@Nullable Object o) {
                return false;
            }

            @Override
            public boolean containsAll(@Nullable Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(@Nullable Collection<? extends Rating> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, @NonNull Collection<? extends Rating> c) {
                return false;
            }

            @Override
            public boolean removeAll(@Nullable Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(@Nullable Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Rating get(int index) {
                return null;
            }

            @Override
            public Rating set(int index, Rating element) {
                return null;
            }

            @Override
            public void add(int index, Rating element) {

            }

            @Override
            public Rating remove(int index) {
                return null;
            }

            @Override
            public int indexOf(@Nullable Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(@Nullable Object o) {
                return 0;
            }

            @NonNull
            @Override
            public ListIterator<Rating> listIterator() {
                return null;
            }

            @NonNull
            @Override
            public ListIterator<Rating> listIterator(int index) {
                return null;
            }

            @NonNull
            @Override
            public List<Rating> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
    }
}
