package com.aras.modir.imdbmvvm.ModelForDatabase.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.IMDBPojo;

@Dao
public interface IMDBPojoDao extends BaseDao<IMDBPojo> {

    @Query("SELECT * FROM IMDBPojo WHERE title = :title")
    LiveData<IMDBPojo> findByTitle(String title);

    @Query("SELECT * FROM IMDBPojo")
    LiveData<IMDBPojo> getAllIMDB();

    @Query("SELECT COUNT(*) FROM IMDBPojo")
    int getNumberofRows();

}
