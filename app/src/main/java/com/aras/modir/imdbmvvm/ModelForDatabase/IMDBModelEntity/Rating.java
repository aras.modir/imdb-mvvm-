
package com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Rating {

    @PrimaryKey
    @NonNull
    @SerializedName("Source")
    @ColumnInfo(name = "source")
    private String source;

    @SerializedName("Value")
    @ColumnInfo(name = "value")
    private String value;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
