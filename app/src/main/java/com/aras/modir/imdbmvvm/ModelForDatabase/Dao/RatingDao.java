package com.aras.modir.imdbmvvm.ModelForDatabase.Dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.aras.modir.imdbmvvm.ModelForDatabase.IMDBModelEntity.Rating;

import java.util.List;

@Dao
public interface RatingDao {

    @Query("SELECT * FROM Rating WHERE source = :source")
    LiveData<List<Rating>> findBySource(String source);

    @Query("SELECT * FROM Rating")
    LiveData<List<Rating>> getAllRating();

    @Query("SELECT COUNT(*) FROM Rating")
    int getNumberofRows();
}
